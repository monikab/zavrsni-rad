import React from 'react';
import './App.scss';
import Project from './pages/Project'
import Sidebar from './components/Sidebar'
import Dashboard from './pages/Dashboard';

function App() {
  return (
    <div className="App">
    
      <Sidebar></Sidebar>


      <Dashboard></Dashboard>
    </div>
  );
}

export default App;
