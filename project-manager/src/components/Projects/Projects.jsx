import React from 'react'
import {
    useTable,
    usePagination,
    useSortBy,
    useFilters,
    useGroupBy,
    useExpanded,
    useRowSelect,
  } from 'react-table'

import makeData from './makeData'

import './projects.scss'

function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: {
        pageIndex,
        pageSize,
        sortBy,
        groupBy,
        expanded,
        filters,
        selectedRowIds,
      },
  } = useTable({
    columns,
    data,
  },
  usePagination,)

  // Render the UI for your table
  return (
      <>
    <table {...getTableProps()} className="table">
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {page.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()} className="table-row">
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()} className="table-cell">{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </table>
          <div className="pagination">
          <button className="pagination-btn" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {'<<'}
          </button>{' '}
          <button className="pagination-btn" onClick={() => previousPage()} disabled={!canPreviousPage}>
            {'<'}
          </button>{' '}
          <button className="pagination-btn" onClick={() => nextPage()} disabled={!canNextPage}>
            {'>'}
          </button>{' '}
          <button className="pagination-btn" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
            {'>>'}
          </button>{' '}
          <span>
            Page{' '}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{' '}
          </span>
         
          <select
            value={pageSize}
            onChange={e => {
              setPageSize(Number(e.target.value))
            }}
          >
            {[5, 10, 20, 50].map(pageSize => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </select>
        </div>
       
        </>
  )
}

function Projects() {
  const columns = React.useMemo(
    () => [
      
            {
                Header: "ID",
                accessor: 'projectId'
            },
            {
                Header: "Name",
                accessor: 'projectName'
            },
            {
                Header: "Date started",
                accessor: 'projectDate'
            },
            {
                Header: "Project manager",
                accessor: 'projectManager'
            },
            {
                Header: "Category",
                accessor: 'projectCategory'
            }
       
     
    ],
    []
  )

  const data = React.useMemo(() => makeData(55), [])

  return (

      <Table columns={columns} data={data} />

  )
}

export default Projects
