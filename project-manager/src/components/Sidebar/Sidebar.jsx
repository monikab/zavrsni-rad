import React from 'react'
import Project from '../../pages/Project'
import SidebarItem from '../SidebarItem'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './sidebar.scss';

function Sidebar() {
    return (
      <Router>
        <div className="sidebar">
          <h1>App</h1>
         <Link className="sidebar-link" to="/">Dashboard</Link>
         <Link className="sidebar-link" to="/users">Users</Link>
         <Link className="sidebar-link" to="/logout">Log out</Link>


          
        </div>
      </Router>
    )
}

export default Sidebar
