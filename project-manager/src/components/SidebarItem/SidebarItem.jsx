import React from 'react'
import logo from '../../logo.png'


function SidebarItem(props) {
    return (
      <a className="sidebar-link" href=""><h2>{props.value}</h2></a>
    )
}

export default SidebarItem