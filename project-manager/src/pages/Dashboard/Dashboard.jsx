import React from 'react'
import './dashboard.scss'
import Projects from '../../components/Projects'
import TaskBoard from '../../components/TaskBoard'

const Dashboard = () => {
    return (
        <div className="dashboard">
            <div className="container">
                <h1 className="dashboard-title">Dashboard</h1>
                
                    {/* <div className="projects-header">
                        <span>Id</span>
                        <span>Name</span>
                        <span>Date added</span>
                        <span>Project manager</span>
                        <span>Category</span>
                    </div>
                    <DashboardItem />
                    <DashboardItem />
                    <DashboardItem /> */}

                    <Projects />

                    {/* <TaskBoard /> */}
         
            </div>
        </div>
    )
}

export default Dashboard